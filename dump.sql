-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for project
CREATE DATABASE IF NOT EXISTS `project` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `project`;

-- Dumping structure for table project.ff
CREATE TABLE IF NOT EXISTS `ff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) DEFAULT NULL,
  `senjata` varchar(30) DEFAULT NULL,
  `efek` varchar(100) DEFAULT NULL,
  `jarak` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Dumping data for table project.ff: ~9 rows (approximately)
/*!40000 ALTER TABLE `ff` DISABLE KEYS */;
INSERT INTO `ff` (`id`, `path`, `senjata`, `efek`, `jarak`) VALUES
	(22, 'image/upload_24-10-2020_061519.jpg', 'M1887 Baru', '+2 Demage', 'Dekat'),
	(25, 'image/upload_25-10-2020_040953.jpg', 'Scar Titan', '+1 Demage', 'Jauh'),
	(31, 'image/upload_27-10-2020_072116.jpg', 'MP5 Vampire', '+2 Rate of fire ', 'Jauh Dan dekat'),
	(32, 'image/upload_27-10-2020_072827.jpg', 'Scar Titan', '+1 Demage', 'Jauh'),
	(33, 'image/upload_27-10-2020_072949.jpg', 'Famas Stardom', '+2 Acuracy', 'Jauh'),
	(35, 'image/upload_27-10-2020_073405.jpg', 'Famas Metalic', '+2 Acuracy', 'Jauh'),
	(36, 'image/upload_27-10-2020_074024.gif', 'AWM bluedia', '+4 Demage', 'Jauh'),
	(38, 'image/upload_27-10-2020_075105.jpg', 'MP5 Pink', '+2 Rate Of Fire', 'Jauh dan Dekat'),
	(39, 'image/upload_27-10-2020_075748.jpg', 'space12 santase', '+2 Demage', 'Dekat');
/*!40000 ALTER TABLE `ff` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
