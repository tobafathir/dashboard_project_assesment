<?php
include 'connection.php';

$siswa=$db->query("select * from ff");
$data_siswa=$siswa->fetchAll();

if(isset($_POST['search']))
{
	$filter=$_POST['search'];
	$search=$db->prepare("select * from ff where senjata=? or efek=? or jarak=?"); // PDO statement
	$search->bindValue(1,$filter,PDO::PARAM_STR);
	$search->bindValue(2,$filter,pdo::PARAM_STR);
	$search->bindValue(3,$filter,pdo::PARAM_STR);
	$search->execute();     //Execution of PDO statement
	
	$tampil_data=$search->fetchAll(); //Result from PDO statement
	$row=$search->rowCount(); //Result from PDO statement
	
}

$temp_arr=[];

foreach ($siswa as $key) {
	$temp_arr[]=$key[0];
}
$pilihan=array_unique($temp_arr);

// Blok filter
$tampilkan_nama=[];
if(isset($_POST['filter']))
{
	// echo "tes";
	var_dump($_POST['filter']);
	exit;
	$filter=$_POST['filter'];
	if($filter == "")
	{
		$tampilkan_siswa=$merk;
	}else{
		foreach($nama as $key)
		{
			if($key[0] == $filter){
				$tampilkan_siswa[]=[$key[0],$key[1],$key[2]];
			}
		}
	}
}else{
	$tampilkan_siswa=$siswa;
}
$kuota = 100;
$temp_data= count($data_siswa);
$temp_kosong = $kuota - $temp_data;
?>
<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
	
	<title>Dashboard</title>
	<style>
		#content{
			width: 80%;
			float: left;
		}
		#sidbar{
			width: 20%;	
			float: left;
			text-align: center;
		}
		.rounded-circle{
			height: 75px;
			width: 75px;
		}
		hr{
			background: grey;
		}
		#nav{
            position: fixed;
            top : 0;
            left :0;
            right :0;
        }
		body{
			background-color: rgb(56, 75, 75);
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-dark" id="nav"  style="z-index: 1;">
		<a class="navbar-brand text-light" href="#">Dashboard Free Fire</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#"><span class="sr-only">(current)</span></a>
				</li>
			</ul>
			<form method="post" class="form-inline my-2 my-lg-0" action="dashboard.php">
				<input class="form-control mr-sm-2" type="search" name="search"  placeholder="Search" aria-label="Search">
				<button type="submit" class="btn btn-secondary">
					<i class="fas fa-search"></i>
				</button>
			</form>
		</div>
	</nav>
	<br>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-12 col-md-6">
				<div class="card text-center bg-dark mt-3 border" style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title text-light">Kuota Senjata</h5>
						<p class="card-text text-light"><?= $kuota; ?></p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-sm-12 col-md-6">
				<div class="card text-center bg-dark mt-3 border" style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title text-light">Data Yang Sudah ada</h5>
						<p class="card-text text-light"><?= $temp_data; ?></p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-sm-12 col-md-12 mx-auto">
				<div class="card text-center bg-dark mt-3  border" style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title text-light">Data Kosong</h5>
						<p class="card-text text-light"><?= $temp_kosong; ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row m-3">
		<div id="sidbar" class="bg-dark border border-secondary">
			<div>
				<img src="image/apk.png" alt="APk ff" class="rounded-circle mt-3">
				<br>
				<hr>
				<h3>
					<a href="home.php" style="text-decoration: none;"><i class="fas fa-home"></i> HOME</a>
				</h3>
				<hr>
				<a href="map.html" style="text-decoration: none;"><i class="fas fa-map"></i> LIHAT MAP</a>
				<br>
				<hr>
				<a href="character.html" style="text-decoration: none;"><i class="fas fa-user-secret"></i> CARACTER</a>
				<hr>
			</div>
		</div>	
		<div id="content" >
			<h3 class="text-center" style="color: black;"><i class="fas fa-columns"></i> 		Data Senjata 	<i class="fas fa-columns"></i></h3>
			<div class="col mx-auto">
				<?php if(isset($row)):?>
				<div class="alert alert-primary alert-dismissible fade show" role="alert">
					<p class="lead"><?php echo $row;?> Data Di Temukan</p>
					<button type="button" class="close" data-dismiss="alert" aria-label="close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<?php endif ;?>
				<table class="table table-striped border">
					  <caption>&copy; by Toba Fathir</caption>
					<thead class="bg-dark">
						<tr>
							<th scope="col" class="text-light">Nama Senjata</th>
							<th scope="col" class="text-light">Efek</th>
							<th scope="col" class="text-light">Jarak</th>
						</tr>
					</thead>
					<tbody class="bg-secondary ">
						<?php foreach($data_siswa as $key): ?>
						<tr>
							<td><?php echo $key['senjata']; ?></td>
							<td><?php echo $key['efek']; ?></td>
							<td><?php echo $key['jarak']; ?></td>	
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		
		
		<!-- Optional JavaScript; choose one of the two! -->
		
		<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
		
		<!-- Option 2: jQuery, Popper.js, and Bootstrap JS
			<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
			<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
			<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
		-->
	</body>
	</html>